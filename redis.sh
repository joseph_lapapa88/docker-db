#!/bin/bash
docker stop queue && docker rm queue

docker run --name queue -p 6370:6379 redis

# docker exec -it queue /usr/local/bin/redis-cli
# docker exec -it queue /bin/bash
# redis-cli config set notify-keyspace-events Ex
